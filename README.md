# iitc-basemap-blank

A better version of core blank base map plugin:

* Better performance
* Infinite zoom

## Install URL

[https://gitlab.com/l1bbcsg/iitc-basemap-blank/raw/master/iitc-basemap-blank.user.js](https://gitlab.com/l1bbcsg/iitc-basemap-blank/raw/master/iitc-basemap-blank.user.js)

## Legacy IITC

Only IITC-CE is supported, old version that should work with legacy IITC is available under [`iitc-legacy`](https://gitlab.com/l1bbcsg/iitc-basemap-blank/-/tags/iitc-legacy) tag. Install: [https://gitlab.com/l1bbcsg/iitc-basemap-blank/raw/iitc-legacy/iitc-basemap-blank.user.js](https://gitlab.com/l1bbcsg/iitc-basemap-blank/raw/iitc-legacy/iitc-basemap-blank.user.js)
