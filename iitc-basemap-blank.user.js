// ==UserScript==
// @id             iitc-plugin-basemap-blank
// @name           IITC plugin: Blank map with infinite zoom
// @author         @l1bbcsg
// @category       Map Tiles
// @version        2.0.0
// @namespace      https://github.com/jonatkins/ingress-intel-total-conversion
// @updateURL      https://gitlab.com/l1bbcsg/iitc-basemap-blank/raw/master/iitc-basemap-blank.user.js
// @description    Adds a blank map layer - no roads or other features. Allows for infinite zoom.
// @match          *://ingress.com/intel
// @match          *://www.ingress.com/intel
// @match          *://intel.ingress.com/*
// @include        /^https?\:\/\/(www\.)?ingress.com\/intel$/
// @include        /^https?\:\/\/intel.ingress.com/
// @grant          none
// ==/UserScript==

(function() {
	const NAME = 'iitc-basemap-blank';
	const VERSION = '2.0.0.20220204';

	if (typeof window.plugin !== 'function') {
		window.plugin = function() {};
	}

	window.plugin.mapTileBlank = function() {};

	window.plugin.mapTileBlank.colors = {
		White: '#FFFFFF',
		Black: '#000000'
	};

	window.plugin.mapTileBlank.addLayer = function() {
		window.plugin.mapTileBlank.BlankLayer = L.Layer.extend({
			options: {},

			initialize: function(color) {
				this._color = color;
				this._element = null;
				this._map = null;
			},

			onAdd: function() {
				this._map = map;

				this._element = document.createElement('div');
				this._element.style.position = 'absolute';
				this._element.style.left = '0';
				this._element.style.top = '0';
				this._element.style.right = '0';
				this._element.style.bottom = '0';
				this._element.style.backgroundColor = this._color;

				this._map.getContainer().appendChild(this._element);
			},

			onRemove: function() {
				this._map.getContainer().removeChild(this._element);
			}
		});

		for (const [name, color] of Object.entries(window.plugin.mapTileBlank.colors))
			layerChooser.addBaseLayer(
				new window.plugin.mapTileBlank.BlankLayer(color),
				"Blank Map (" + name + ")"
			);
	};

	const setup = window.plugin.mapTileBlank.addLayer;

	setup.info = {
		buildName: NAME + ':' + VERSION,
		dateTimeVersion: VERSION,
		pluginId: NAME,
	};

	if (!window.bootPlugins) {
		window.bootPlugins = [];
	}
	window.bootPlugins.push(setup);

	if (window.iitcLoaded && typeof setup === 'function') {
		setup();
	}
})();
